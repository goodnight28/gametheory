import Foundation

public final class LB7 {
    
    // MARK: - Private properties
    private let a: Double
    private let b: Double
    private let c: Double
    private let d: Double
    private let e: Double
    private let cPrint: Print
    private let formule: MatrixFormule
    
    // MARK: - Init
    public init(
        a: Double,
        b: Double,
        c: Double,
        d: Double,
        e: Double,
        cPrint: Print,
        formule: MatrixFormule)
    {
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.e = e
        self.cPrint = cPrint
        self.formule = formule
        
        checkCorrectness()
    }
    
    // MARK: - Public
    public func analyticalMethod() {
        print("Аналитическое решение:\n")
        
        let y = (c * d + 2 * a * e) / (4 * a * b - c * c)
        let x = -1 * ((c * y + d) / (2 * a))
        let H = h(x: x, y: y)
        
        print(String(format:"x = %3.3f | y = %3.3f | H = %3.3f", x, y, H))
    }
    
    public func numericalSolution() {
        print("\nЧисленное решение:")
        
        for N in 2..<11 {
            print("\nN = \(N)")
            
            let matrix = makeMatrix(N: N)
            
            if findSaddlePoint(matrix: matrix, N: N) == nil {
                let (x_k, y_k) = brownRobinsonMethod(matrix: matrix)
                
                if let x = x_k.max(), let y = y_k.max() {
                    let H = h(x: x, y: y)
                    print(String(format:"x = %3.3f | y = %3.3f | H = %3.3f", x, y, H))
                }
            }
        }
    }

    // MARK: - Private
    private func checkCorrectness() {
        let Hxx = 2 * a
        let Hyy = 2 * b
        
        if Hxx < 0 && Hyy > 0 {
            print("Игра выпукло-вогнутая\n")
        }
    }
    
    private func h(
        x: Double,
        y: Double)
        -> Double
    {
        a * x * x + b * y * y + c * x * y + d * x + e * y
    }
    
    private func makeMatrix(
        N: Int)
        -> [[Double]]
    {
        var matrix = [[Double]]()
        
        for i in 0..<(N + 1) {
            let x = Double(i)/Double(N)
            matrix.append([])
        
            for j in 0..<(N + 1){
                let y = Double(j)/Double(N)
                matrix[i].append(h(x: x, y: y))
            }
        }
        cPrint.matrix(matrix)
        
        return matrix
    }
    
    func findSaddlePoint(
        matrix: [[Double]],
        N: Int)
        -> (Double, Double, Double)?
    {
        var rowMin = [Double] (repeating: -10000.0, count: N + 1)
        var colMax = [Double] (repeating: -10000.0, count: N + 1)
        
        for i in 0..<(N + 1) {
            for j in 0..<(N + 1){
                rowMin[j] = min(rowMin[j], matrix[j][i])
                colMax[i] = max(colMax[i], matrix[j][i])
            }
        }
        
        for i in 0..<(N + 1) {
            for j in 0..<(N + 1) {
                if matrix[j][i] == rowMin[j] && matrix[j][i] == colMax[i] {
                    print("Есть седловая точка:")
                    
                    let x = Double(j)/Double(N)
                    let y = Double(i)/Double(N)
                    
                    return (x, y, matrix[j][i])
                }
            }
        }
        
        print("Седловых точек нет!")
        return nil
    }

    private func brownRobinsonMethod(
        matrix: [[Double]])
        -> ([Double], [Double])
    {
        print("Метод Брауна–Робинсона:\n")
        
        var plotTop = [Double]()
        var plotBot = [Double]()
        let sizeMatrixLines = matrix.count
        var round = 1.0
        
        var choiceA = Int.random(in: 0..<sizeMatrixLines - 1)
        var choiceB = Int.random(in: 0..<sizeMatrixLines - 1)
        
        var xRound = formule.getVector(
            valueInVector: 0,
            sizeMatrixLines: sizeMatrixLines
        )
        var yRound = formule.getVector(
            valueInVector: 0,
            sizeMatrixLines: sizeMatrixLines
        )
        
        xRound[choiceA] += 1
        yRound[choiceB] += 1
        
        var winA = matrix.map { $0[choiceA] }
        var losseB = matrix[choiceB].map { $0 }
        
        var vTop = (winA.max() ?? 1) / round
        var vBot = (losseB.min() ?? 1) / round
        
        var vTopMin = vTop
        var vBoTMax = vBot
        
        var eps = vTop - vBot
        
        plotBot.append(vBot)
        plotTop.append(vTop)
        
        while eps >= 0.1 {
            let iLen = indexes(
                vector: losseB,
                value: losseB.min() ?? 1
            )
            choiceA = iLen[Int.random(in: 0..<iLen.count)]
            
            let jLen = indexes(
                vector: winA,
                value: winA.max() ?? 1
            )
            choiceB = jLen[Int.random(in: 0..<jLen.count)]
            
            xRound[choiceA] += 1
            yRound[choiceB] += 1
            
            winA = formule.sumVectors(
                firstVector: winA,
                secondVector: matrix.map { $0[choiceA] }
            )
            losseB = formule.sumVectors(
                firstVector: losseB,
                secondVector: matrix[choiceB].map { $0 }
            )
            
            round += 1
            
            vTop = (winA.max() ?? 1) / round
            vBot = (losseB.min() ?? 1) / round
            
            if vTopMin > vTop {
                vTopMin = vTop
            }
            if vBoTMax < vBot {
                vBoTMax = vBot
            }
            eps = vTopMin - vBoTMax
            
            plotTop.append(vTop)
            plotBot.append(vBot)
        }

        yRound = yRound.map { $0 / round }
        xRound = xRound.map { $0 / round }
        
        return (yRound, xRound)
    }
    
    private func indexes(
        vector: [Double],
        value: Double)
        -> [Int]
    {
        return vector.enumerated().compactMap {
            if $1 == value {
                return $0
            } else {
                return nil
            }
        }
    }
}
