let lb7 = LB7(
    a: -5,
    b: 10/3,
    c: 10,
    d: -2,
    e: -8,
    cPrint: Print(),
    formule: MatrixFormule()
)

lb7.analyticalMethod()
lb7.numericalSolution()
