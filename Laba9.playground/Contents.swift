let lb9 = LB9(formule: MatrixFormule())

lb9.balanceResult(
    biMatrix:
        [
            [[1.0, 1.0], [0.5, 2.0]],
            [[2.0, 0.5], [0.0, 0.0]]
        ]
)

lb9.balanceResult(
    biMatrix:
        [
            [[4.0, 1.0], [0.0, 0.0]],
            [[0.0, 0.0], [1.0, 4.0]]
        ]
)

lb9.balanceResult(
    biMatrix:
        [
            [[-5.0, -5.0], [0.5, -10.0]],
            [[-10.0, 0.0], [-1.0, -1.0]]
        ]
)

lb9.balanceResult(
    biMatrix: lb9.generateBiMatrix(
        min: -100,
        max: 100,
        dim: 10
    )
)

lb9.balanceResult(
    biMatrix:
        [
            [[6, 7], [8, 4]],
            [[2, 1], [9, 3]]
        ]
)

lb9.analyticMethod(
    biMatrix:
        [
            [[6, 7], [8, 4]],
            [[2, 1], [9, 3]]
        ]
)
