import Foundation

public final class LB9 {
    
    // MARK: - Private properties
    private let formule: MatrixFormule
    
    // MARK: - Init
    public init(
        formule: MatrixFormule)
    {
        self.formule = formule
    }
    
    // MARK: - Public
    public func analyticMethod(
        biMatrix: [[[Double]]])
    {
        print("Аналитический метод:")
        
        let matrixDim = biMatrix[0].count
        let u = formule.getMatrix(
            lines: matrixDim,
            columes: matrixDim,
            value: 1
        )
        
        let tmpA = formule.matrixProduct(
            matrixA: u,
            matrixB: formule.matrixProduct(
                matrixA: formule.inverse(biMatrix[0]),
                matrixB: formule.transpose(u)
            )
        )
        let tmpB = formule.matrixProduct(
            matrixA: u,
            matrixB: formule.matrixProduct(
                matrixA: formule.inverse(biMatrix[0]),
                matrixB: formule.transpose(u)
            )
        )
        
        let vX = tmpA.map { formule.divide(dividend: 1, divider: $0) }
        let vY = tmpB.map { formule.divide(dividend: 1, divider: $0) }
        
        print(
            String(
                format: "VX = %2.2f | VY = %2.2f",
                vX[0][0],
                vY[0][0]
            )
        )
        
        let x = formule.matrixProduct(
            matrixA: vX,
            matrixB: formule.matrixProduct(
                matrixA: formule.inverse(biMatrix[0]),
                matrixB: formule.transpose(u)
            )
        )
        let y = formule.matrixProduct(
            matrixA: vY,
            matrixB: formule.matrixProduct(
                matrixA: u,
                matrixB: formule.inverse(biMatrix[0])
            )
        )
        
        for value in formule.transpose(x)[0] {
            print(
                String(
                    format: "X = %0.2f",
                    value
                )
            )
        }
        for value in y[0] {
            print(
                String(
                    format: "Y = %0.2f",
                    value
                )
            )
        }
        
    }
    
    public func balanceResult(
        biMatrix: [[[Double]]])
    {
        let matrixDim = biMatrix[0].count
        
        for i in 0..<matrixDim {
            var line = [String]()
            
            for j in 0..<matrixDim {
                let isNash = nash(biMatrix: biMatrix, i: i, j: j)
                let isPareto = pareto(biMatrix: biMatrix, i: i, j: j)
                let point = String(
                    format: "%3.3f / %3.3f",
                    biMatrix[i][j][0],
                    biMatrix[i][j][1]
                )
                
                if isNash && isPareto {
                    line.append("|\(point)|")
                } else if isPareto {
                    line.append("&\(point)&")
                } else if isNash {
                    line.append("*\(point)*")
                } else {
                    line.append("(\(point))")
                }
            }
            
            print(line)
        }
    }
    
    public func generateBiMatrix(
        min: Double,
        max: Double,
        dim: Int)
        -> [[[Double]]]
    {
        var biMatrix = [[[Double]]]()
        
        for i in 0..<dim {
            var biArray = [[Double]]()
            
            for j in 0..<dim {
                biArray.append(
                    [Double.random(in: min..<max),
                     Double.random(in: min..<max)]
                )
            }
            
            biMatrix.append(biArray)
        }
        
        return biMatrix
    }
    
    // MARK: - Private
    private func nash(
        biMatrix: [[[Double]]],
        i: Int,
        j: Int)
        -> Bool
    {
        let matrixDim = biMatrix[0].count
        var bestAStrategy = true
        var bestBStrategy = true
        
        for iterI in 0..<matrixDim {
            if biMatrix[iterI][j][0] > biMatrix[i][j][0] {
                bestBStrategy = false
            }
            if biMatrix[i][iterI][1] > biMatrix[i][j][1] {
                bestAStrategy = false
            }
        }
        
        return bestAStrategy && bestBStrategy
    }
    
    private func pareto(
        biMatrix: [[[Double]]],
        i: Int,
        j: Int)
        -> Bool
    {
        let matrixDim = biMatrix[0].count
        var bestStrategy = true
        
        for iIter in 0..<matrixDim {
            for jIter in 0..<matrixDim {
                if
                    (biMatrix[iIter][jIter][0] > biMatrix[i][j][0] &&
                    biMatrix[iIter][jIter][1] >= biMatrix[i][j][1]) ||
                    (biMatrix[iIter][jIter][1] > biMatrix[i][j][1] &&
                    biMatrix[iIter][jIter][0] >= biMatrix[i][j][0])
                {
                    bestStrategy = false
                }
            }
        }
        
        return bestStrategy
    }
}
