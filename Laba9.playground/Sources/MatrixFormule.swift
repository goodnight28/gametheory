import Foundation

public class MatrixFormule {
    // MARK: - Init
    public init() {}
    
    // MARK: - Public
    public func getMatrix(
        lines: Int,
        columes: Int,
        value: Double = 0)
        -> [[Double]]
    {
        var matrix = [[Double]]()
        
        for _ in 0..<lines {
            var line = [Double]()
            
            for _ in 0..<columes {
                line.append(value)
            }
            
            matrix.append(line)
        }
        
        return matrix
    }

    public func getVector(
        valueInVector: Double,
        sizeMatrixLines: Int)
        -> [Double]
    {
        var vector = [Double]()
        
        for _ in 0..<sizeMatrixLines {
            vector.append(valueInVector)
        }
        
        return vector
    }

    public func inverse(
        _ matrix: [[Double]])
        -> [[Double]]
    {
        var matrix = matrix
        var idrow = Array(repeating: 0.0, count: matrix.count)
        idrow[0] = 1.0
        for row in 0..<matrix.count {
            matrix[row] += idrow
            idrow.insert(0.0, at:0)
            idrow.removeLast()
        }
        
        for row1 in 0..<matrix.count {
            for row2 in row1..<matrix.count {
                if abs(matrix[row1][row1]) < abs(matrix[row2][row2]) {
                    (matrix[row1],matrix[row2]) = (matrix[row2],matrix[row1])
                }
            }
        }
        
        for pivot in 0..<matrix.count {
            let arg = 1.0 / matrix[pivot][pivot]
            for col in pivot..<matrix[pivot].count {
                matrix[pivot][col] *= arg
            }
            
            for row in (pivot+1)..<matrix.count {
                let arg = matrix[row][pivot] / matrix[pivot][pivot]
                for col in pivot..<matrix[row].count {
                    matrix[row][col] -= arg * matrix[pivot][col]
                }
            }
        }
        
        for pivot in (0..<matrix.count).reversed() {
            for row in 0..<pivot {
                let arg = matrix[row][pivot] / matrix[pivot][pivot]
                for col in pivot..<matrix[row].count {
                    matrix[row][col] -= arg * matrix[pivot][col]
                }
            }
        }
        
        for row in 0..<matrix.count {
            for _ in 0..<matrix.count {
                matrix[row].remove(at:0)
            }
        }
        
        return matrix
    }

    public func transpose(
        _ matrix: [[Double]])
        -> [[Double]]
    {
        let columns = matrix.count
        let rows = matrix.reduce(0) { max($0, $1.count) }

        var result: [[Double]] = []

        for row in 0 ..< rows {
            result.append([])
            for col in 0 ..< columns {
                if row < matrix[col].count {
                    result[row].append(matrix[col][row])
                } else {
                    result[row].append(0)
                }
            }
        }

        return result
    }

    public func matrixProduct(
        matrixA: [[Double]],
        matrixB: [[Double]])
        -> [[Double]]
    {
        guard
            let columeSize = matrixB.first?.count
            else { return [] }
        
        var newMatrix = getMatrix(
            lines: matrixA.count,
            columes: columeSize
        )
        
        for counterLine in 0..<matrixA.count {
            for counterColume in 0..<columeSize {
                newMatrix[counterLine][counterColume] = 0
                
                for counterSum in 0..<matrixB.count {
                    newMatrix[counterLine][counterColume] += matrixA[counterLine][counterSum] * matrixB[counterSum][counterColume]
                }
            }
        }
        
        return newMatrix
    }

    public func divide(
        dividend: [Double],
        divider: Double)
        -> [Double]
    {
        return  dividend.map { $0 / divider }
    }
    
    public func divide(
        dividend: Double,
        divider: [Double])
        -> [Double]
    {
        return  divider.map { dividend / $0 }
    }
    
    public func sumVectors(
       firstVector: [Double],
       secondVector: [Double])
       -> [Double]
   {
       return firstVector.enumerated().map { $1 + secondVector[$0] }
   }
}
