import Foundation

public class LB5 {
    // MARK: - Private properties
    private let formule: MatrixFormule
    private let cPrint: Print
    
    // MARK: - Init
    public init(
        formule: MatrixFormule,
        cPrint: Print)
    {
        self.formule = formule
        self.cPrint = cPrint
    }
    
    // MARK: - Public
    public func analyticalMethod(
        matrix: [[Double]])
    {
        print("Аналитический метод ->\n")
        
        let sizeMatrixLines = matrix.count
        let u = formule.getVector(
            valueInVector: 1,
            sizeMatrixLines: sizeMatrixLines
        )
        let matrixInverse = formule.inverse(matrix)
        
        let xNumerator = formule.matrixProduct(
            matrixA: matrixInverse,
            matrixB: formule.transpose([u])
        )
        let xDenominator = formule.matrixProduct(
            matrixA: formule.matrixProduct(
                matrixA: [u],
                matrixB: matrixInverse
            ),
            matrixB:  formule.transpose([u])
        )
        let x = xNumerator.map { ($0.first ?? 1) / (xDenominator.first?.first ?? 1) }
        
        let yNumerator = formule.matrixProduct(
            matrixA: [u],
            matrixB: matrixInverse
        )
        let yDenominator = formule.matrixProduct(
            matrixA: formule.matrixProduct(
                matrixA: [u],
                matrixB: matrixInverse
            ),
            matrixB:  formule.transpose([u])
        )
        guard
            let yNumeratorVector = yNumerator.first
            else { return }
        let y = yNumeratorVector.map { $0 / (yDenominator.first?.first ?? 1) }
        
        let value = yDenominator.map { $0.map { 1.0 / $0 } }
        
        print("Оптимальная стратегия игрока 1:")
        cPrint.matrix([y])
        print("Оптимальная стратегия игрока 2:")
        cPrint.matrix([x])
        print("Цена игры: ")
        cPrint.matrix(value)
    }


   

    public func brownRobinsonMethod(
        matrix: [[Double]])
    {
        print("Метод Брауна–Робинсона ->\n")
        
        var plotTop = [Double]()
        var plotBot = [Double]()
        let sizeMatrixLines = matrix.count
        var round = 1.0
        
        var choiceA = Int.random(in: 0..<sizeMatrixLines - 1)
        var choiceB = Int.random(in: 0..<sizeMatrixLines - 1)
        
        var xRound = formule.getVector(
            valueInVector: 0,
            sizeMatrixLines: sizeMatrixLines
        )
        var yRound = formule.getVector(
            valueInVector: 0,
            sizeMatrixLines: sizeMatrixLines
        )
        
        xRound[choiceA] += 1
        yRound[choiceB] += 1
        
        var winA = matrix.map { $0[choiceA] }
        var losseB = matrix[choiceB].map { $0 }
        
        var vTop = (winA.max() ?? 1) / round
        var vBot = (losseB.min() ?? 1) / round
        
        var vTopMin = vTop
        var vBoTMax = vBot
        
        var eps = vTop - vBot
        
        plotBot.append(vBot)
        plotTop.append(vTop)
        
        print(" \(Int(round))  |   \(choiceA)   |   \(choiceB)   |   \(winA)   |   \(losseB)   |   \(String(format: "%.3f", vTop))   |   \(String(format: "%.3f", vBot))   |   \(eps)  ")
        
        while eps >= 0.1 {
            let iLen = indexes(
                vector: losseB,
                value: losseB.min() ?? 1
            )
            choiceA = iLen[Int.random(in: 0..<iLen.count)]
            
            let jLen = indexes(
                vector: winA,
                value: winA.max() ?? 1
            )
            choiceB = jLen[Int.random(in: 0..<jLen.count)]
            
            xRound[choiceA] += 1
            yRound[choiceB] += 1
            
            winA = formule.sumVectors(
                firstVector: winA,
                secondVector: matrix.map { $0[choiceA] }
            )
            losseB = formule.sumVectors(
                firstVector: losseB,
                secondVector: matrix[choiceB].map { $0 }
            )
            
            round += 1
            
            vTop = (winA.max() ?? 1) / round
            vBot = (losseB.min() ?? 1) / round
            
            if vTopMin > vTop {
                vTopMin = vTop
            }
            if vBoTMax < vBot {
                vBoTMax = vBot
            }
            eps = vTopMin - vBoTMax
            
            print(" \(Int(round))  |   \(choiceA)   |   \(choiceB)   |   \(winA)   |   \(losseB)   |   \(String(format: "%.3f", vTop))   |   \(String(format: "%.3f", vBot))   |   \(eps)  ")
            
            plotTop.append(vTop)
            plotBot.append(vBot)
        }

        yRound = yRound.map { $0 / round }
        xRound = xRound.map { $0 / round }
        
        print("Оптимальная стратегия игрока 1:")
        cPrint.matrix([yRound])
        print("Оптимальная стратегия игрока 2:")
        cPrint.matrix([xRound])
        print("Цена игры в промежутке от \(String(format: "%.3f", vBot)) до \(String(format: "%.3f", vTop))")
        print("Количество раундов: \(round)")
    }
    
    // MARK: - Private
    private func indexes(
        vector: [Double],
        value: Double)
        -> [Int]
    {
        return vector.enumerated().compactMap {
            if $1 == value {
                return $0
            } else {
                return nil
            }
        }
    }
}
