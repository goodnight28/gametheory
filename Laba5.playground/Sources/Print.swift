import Foundation

public class Print {
    // MARK: - Init
    public init() {}
    
    // MARK: - Public
    public func matrix(
        _ matrix: [[Double]])
    {
        matrix.enumerated().forEach {
            print("[", terminator: " ")
            
            $1.enumerated().forEach {
                print("\(String(format: "%.3f", $1))", terminator: " ")
            }
            
            print("]")
        }
    }
}
