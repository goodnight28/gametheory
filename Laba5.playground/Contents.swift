import Foundation

let lb5 = LB5(
    formule: MatrixFormule(),
    cPrint: Print()
)

let matrix: [[Double]] = [[17, 4, 9],
                          [0, 16, 9],
                          [12, 2, 19]]

lb5.analyticalMethod(matrix: matrix)
lb5.brownRobinsonMethod(matrix: matrix)
