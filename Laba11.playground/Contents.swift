let Vt = {
    "": 0,
    "1": 4,
    "2": 3,
    "3": 2,
    "4": 3,
    "1 2": 8,
    "1 3": 6,
    "1 4": 7,
    "2 3": 5,
    "2 4": 7,
    "3 4": 6,
    "1 2 3": 10,
    "1 2 4": 12,
    "1 3 4": 10,
    "2 3 4": 9,
    "1 2 3 4": 13,
}

let I = "1 2 3 4"

func getVectorShepli(game: [String: String]) {
    let vector = Array(repeating: 0, count: 5)
    
    for i in 1..<5 {
        for team in game.keys {
            let vS =
        }
    }
}

func printGame(game: [String: String]) {
    for (k, v) in game.items {
        if k != "" {
            print("v({0}) = {1};".format(k, v))
        }
    }
}

func getVectorShepli(game: [String: String]) {
    let allTeam = [set(k.split(" ")) for k in game.keys if k != ""]
    var vector = Array.zeros(5)
    
    for i in 1..<5 {
        for team in allTeam {
            let v_s = " ".join(sorted(list(team)))
            let v_s_i = " ".join(sorted(list(team.difference(set(str(i))))))
            let vector[i] += Math.factorial(len(team) - 1) * Math.factorial(4 - len(team)) * (game[v_s] - game[v_s_i])
        }
    }
    return (vector / np.math.factorial(4))[1:5]
}


func checkSuperAdditive(
    game: [String: String])
    -> (Bool, [String])
{
    var isSuperAdditive = true
    var errorCoal = [String]()
    let allTeams = [set(k.split(" ")) for k in game.keys if k != "" and k != I]
    
    for (s, t) in combinations(allTeams, 2) {
        
        if !(s & t) {
            let keyS = " ".join(sorted(list(s)))
            let keyT = " ".join(sorted(list(t)))
            let keySt = " ".join(sorted(list(s | t)))
            
            if game[keySt] < game[keyS] + game[keyT] {
                    is_super_additive = False
                    error_coal.append(keySt)
            }
        }
    }
    
    return (isSuperAdditive, errorCoal)
}

func checkConvex(game: [String: String]) -> Bool {
    var isConvex = true
    let allTeams = [set(k.split(" ")) for k in game.keys() if k != "" and k != I]
    
    for (s, t) in combinations(allTeams, 2) {
        if !(s & t) {
            keyS = " ".join(sorted(list(s)))
            keyT = " ".join(sorted(list(t)))
            keyST = " ".join(sorted(list(s | t)))
            keySt = " ".join(sorted(list(s & t)))
            if game[keySt] + game[keyST] < game[keyS] + game[keyT] {
                isConvex = false
            }
        }
    }
    
    return isConvex
}

while (true) {
    print("Игра:")
    printGame(game: Vt)
    let isSA, err = checkSuperAdditive(game: Vt)
    
    if !isSA {
        print("Игра не супераддитивна на наборе {0}".format(err))
    } else {
        print("Игра супераддитивна")
    }
    
    let isConv = checkConvex(game: Vt)
    if !isConv {
        print("Игра не выпукла")
    } else {
        print("Игра выпукла")
    }
    
    let sheply = getVectorShepli(game: Vt)
    print("Вектор Шепли ", " ".join(["{0:.2f}".format(x) for x in sheply]))
    let sheplySum = sum(sheply)
    print("Групповая рационализация:")
    
    if sheply_sum == Vt[I] {
        print("{0:.2f}".format(sheplySum), "=", Vt[I])
        print("Выполняется")
    } else {
        print("{0:.2f}".format(sheplySum), "!=", Vt[I])
        print("Не выполняется")
    }
    
    print("Индивидуальная рационализация:")
    for i in 1..<5 {
        print("x{0}:".format(i), "{0:.2f}".format(sheply[i-1]), ">=", Vt[str(i)])
    }
    if !isSA {
        print("Изменим игру".format(err))
        for key in set(err) {
            Vt[key] += 1
        }
    } else {
        break
    }
    print("\n\n")
}
