import UIKit
import Foundation

let rk1 = RK1(
    cPrint: Print()
)

rk1.game(
    pointQuantity: 10,
    detectionRadius: 1,
    sphereRadius: 10,
    count: 100
)
