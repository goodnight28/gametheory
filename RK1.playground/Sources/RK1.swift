import Foundation

public class RK1 {
    // MARK: Private properties
    let cPrint: Print
    
    // MARK: - Init
    public init(
        cPrint: Print)
    {
        self.cPrint = cPrint
    }
    
    // MARK: Public
    public func game(
        pointQuantity: Double,
        detectionRadius: Double,
        sphereRadius: Double,
        count: Int)
    {
        var win_a: Double = 0
        
        for _ in 0..<count {
            let isWin = winFunction(
                pointQuantity: pointQuantity,
                detectionRadius: detectionRadius,
                sphereRadius: sphereRadius
            )
            if isWin {
                win_a += 1
            }
        }
        
        let numericalResult = win_a / Double(count)
        let analytic = analyticMethod(
            pointQuantity: pointQuantity,
            detectionRadius: detectionRadius,
            sphereRadius: sphereRadius
        )
        
        print("Первый игрок выбирает \(pointQuantity) точек, радиус \(detectionRadius)")
        print("Выигрыш игрока 1: \(String(format: "%.3f", numericalResult))")
        print("Выигрыш игрока 2: \(1 - numericalResult)")
        print("Аналитического метода: \(String(format: "%.3f", analytic))")
        print("Разница: \(abs(analytic - numericalResult))")
    }
    
    // MARK: - Private
    private func getRandomPoint(
        sphereRadius: Double)
        -> [Double]
    {
        let z: Double = Double.random(in: -sphereRadius..<sphereRadius)
        let phi = Double.random(in: 0..<(2 * Double.pi))
        let r = sphereRadius * sqrt(1 - pow(z, 2) / pow(sphereRadius, 2))
        let x: Double = cos(phi) * r
        let y: Double = sin(phi) * r
        
        return [x, y, z]
    }

    private func getPointsArray(
        pointQuantity: Double,
        sphereRadius: Double)
        -> [[Double]]
    {
        var pointsList: [[Double]] = []
        
        for _ in 0..<Int(pointQuantity) {
            pointsList.append(getRandomPoint(sphereRadius: sphereRadius))
        }
        
        return pointsList
    }
    
    private func analyticMethod(
        pointQuantity: Double,
        detectionRadius: Double,
        sphereRadius: Double)
        -> Double
    {
        if detectionRadius > sphereRadius {
            return 1 
        } else {
            return pointQuantity / 2 * (1 - sqrt(1 - pow(detectionRadius / sphereRadius, 2)))
        }
    }

    private func winFunction(
        pointQuantity: Double,
        detectionRadius: Double,
        sphereRadius: Double)
        -> Bool
    {
        var win = false
        
        let playerB = getRandomPoint(sphereRadius: sphereRadius)
        let playerA = getPointsArray(
            pointQuantity: pointQuantity,
            sphereRadius: sphereRadius
        )
        
        print("Точка, где прячется игрок:")
        cPrint.matrix([playerB])
        print("Точки поиска:")
        cPrint.matrix(playerA)
        
        let maxDistance = detectionRadius
        
        playerA.forEach {
            let distance = sqrt(pow(($0[0] - playerB[0]), 2) + pow(($0[1] - playerB[1]), 2) + pow(($0[2] - playerB[2]), 2))
            
            if distance <= maxDistance {
                win = true
            }
        }
        
        win ? print("Победил 1 игрок") : print("Победил 2 игрок")
        
        return win
    }
}
