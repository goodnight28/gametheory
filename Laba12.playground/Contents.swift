let dim = 10
let epsilon = 10e-6
let controlLow = 0
let controlHigh = 100


func getInitOpinions(
    controlLow: Float,
    controlHight: Float,
    dimension: Float)
    -> [Float]
{
    random.randint(controlLow, controlHight, dimension)
}


func printMatrix(matrix: [[Float]]) {
    print("Матрица доверия:")
    print("\n".join(", ".join("{0:.3f}".format(x) for x in row) for row in matrix))
}


func getTrustMatrix(
    dimension: Float)
    -> [[Float]]
{
    var matrix = [[Float]]()
    
    for _ in 0..<dimension {
        let row = np.random.sample(dimension)
        matrix.append(row / row.sum())
    }
    
    printMatrix(matrix)
    return array(matrix)
}


func matrixReduce(
    matrix: [[Float]],
    eps: [Float],
    opinions: Float)
    -> (Float, Int)
{
    var i = 0
    var reduced = true
    var opinions = opinions
    
    while reduced {
        i += 1
        let changedOpinions = matrix.dot(opinions).transpose()
        
        if all(x <= eps for x in np.abs(opinions - changed_opinions)) {
            reduced = false
        }
        
        opinions = changedOpinions
    }
    
    return (opinions, i)
}


func solve(
    matrix: [[Float]],
    eps: Float,
    opinions: Float)
{
    let (opResult, iterCount) = matrixReduce(matrix: matrix, eps: eps, opinions: opinions)
    print()
    print("Начальные мнения агентов: X(0) =", opinions)
    print("Количество итераций:", iterCount)
    print("Результирующее мнение агентов без влияния: X(t->inf) =", ", ".join("{0:.3f}".format(x) for x in opResult))
    print()
}


func getAgents(dimension: Float) {
    var agents = arange(dimension)
    random.shuffle(agents)
    var (uSize, vSize) = len(agents), len(agents)
    
    while uSize + vSize > len(agents) {
        u_size = np.random.randint(1, len(agents))
        v_size = np.random.randint(1, len(agents))
    }
    
    return (agents[:u_size], agents[u_size:u_size + v_size])
}

func getControls(
    controlLow: Float,
    controlHight: Float,
    sign: Bool)
    -> [Float]
{
    control = random.randint(controlLow, controlHight)
    
    return control if sign else -1 * control
}


func solveInfluence(
    matrix: [[Float]],
    dimension: Float,
    eps: Float)
{
    var (uAgents, vAgents) = getAgents(dimension: dimension)
    print("Агенты первого игрока: {0}".format(sorted(uAgents)))
    print("Агенты второго игрока: {0}".format(sorted(vAgents)))
    uControl = getControls(controlLow: controlLow, controlHigh: controlHight, sign: true)
    vControl = getControls(controlLow: controlLow, controlHigh: controlHight, sign: false)
    print("Сформированное начальное мнение первого игрока: {0:.0f}".format(uControl))
    print("Сформированное начальное мнение второго игрока: {0:.0f}".format(vControl))
    influencedOpinions = initialOpinions
    for number in hstack((vAgents, uAgents)) {
        influencedOpinions[number] = uControl if number in uAgents else vControl
    }
    print("Изначальные мнения с учетом сформированных: X(0) =", influencedOpinions)
    var (resultOpinions, iterCount) = matrixReduce(matrix: matrix, eps: eps, opinions: influencedOpinions)
    print("Количество итераций:", iterCount)
    print("Результирующее мнение: X(t->inf) =", ", ".join("{0:.3f}".format(x) for x in resulOpinions))
}

let initialOpinions = getInitialOpinions(controlLow: controlLow, controlHigh: controlHigh, dimension: dim)
let trustMatrix = getTrustMatrix(dimension: dim)

solve(matrix: trustMatrix, eps: epsilon, opinions: initialOpinions)
solveInfluence(matrix: trust_matrix, dimension: dim, eps: epsilon)
